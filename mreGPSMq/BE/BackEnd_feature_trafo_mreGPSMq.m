function [OPM,OPM_raw,SNR_dc,SNR_ac]= BackEnd_feature_trafo_mreGPSMq(BE,params)
% BackEnd_feature_trafo_mreGPSMq.m  function that transforms envelope power SNRs (modulation features) and
% intensity SNRs (intensity features) into a perceptual measure
% 
% input:
%       BE.signal_inc:         snr-matrix of signal increment (containing short-time modulation and long-time intensity features)
%       BE.signal_dec:         snr-matrix of signal decrement (containing short-time modulation and long-time intensity features)
%       BE.signal_STint_inc:   snr-vector containing increments of short-time intensity features   
%       BE.signal_STint_inc:   snr-vector containing decrements of short-time intensity features 
%       params:                parameters specifying back-end processing
% 
% output:  
%       OPM:                   SNRs transformed onto a continuously perceptual
%                              quality scale with upper and lower limits as
%                              it was described in Biberger et al. (2018)
%       OPM_raw:               SNR transformed onto a continuously
%                              perceptual scale
%       SNR_dc:                intensity based SNRs
%       SNR_ac:                envelope power based SNRs
% 
% Usage: [OPM,OPM_raw,SNR_dc,SNR_ac]= BackEnd_feature_trafo_mreGPSMq(BE,params)
% author: thomas.biberger@uni-oldenburg; 
% date:   2018-05-21
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Thomas Biberger, Jan-Hendrik Fle�ner, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------


signal_inc=BE.SNR_mat_inc;
signal_dec=BE.SNR_mat_dec;
signal_STint_inc=BE.SNR_mat_STint_inc;
signal_STint_dec=BE.SNR_mat_STint_dec;
%% combine increment & decrement SNRs
signal_dec(params.mod_filt_range.start:params.mod_filt_range.end,:) = 10.^(-12/10).*signal_dec(params.mod_filt_range.start:params.mod_filt_range.end,:);

signal_temp = (signal_inc+signal_dec) .* 0.5;
signal_temp(1,:) = (signal_STint_inc(4,:) + signal_STint_dec(4,:)) .* 0.5;

%% combine across auditory and modulation/intensity channels
signal_temp=signal_temp.^2;

% combining dc-channels
signal_temp_dc=signal_temp(1,params.auditory_filt_range.start:params.auditory_filt_range.end);
SNR_dc=sqrt(sum(signal_temp_dc,2));

% combining ac-channels
signal_temp_ac=signal_temp(params.mod_filt_range.start:params.mod_filt_range.end,params.auditory_filt_range.start:params.auditory_filt_range.end);
SNR_periph_ac=sum(signal_temp_ac,2);
SNR_ac=sqrt(sum(SNR_periph_ac,1));

%% transformation of a single SNR-value into a perceptual measure
OPM_raw=10*log10(SNR_dc + SNR_ac);
OPM = min(max(10*log10(SNR_dc + SNR_ac)+4,0),17.6);

end