function[out, out_int, work, dc2mod]=multResGPSM_LpFB_int_SIP_v2(input,gtfb_num,mfb_num,fs,work,signal_rect,params)
% function[out out_int work dc2mod]=multResGPSM_LpFB_int_SIP_v2(input,gtfb_num,mfb_num,fs,dc_norm,work,signal_rect,params,simdef)
% multResGPSM_LpFB_int_SIP.m This function calculates the multi-resolution-based
% envelope power and short-time power
%
%   INPUT:
%       input:            3-dim. input matrix representing the input signal
%                         input(time,audio channels,intensity/modulation channels)
%       gtfb_num:         number of auditory channels
%       mfb_num:          number of modulation channel + 1 intensity
%                         channel
%       fs:               sampling frequency
%       work:
%       signal_rect:      low-pass filtered (150 Hz) hilbert envelope per auditory
%                         channel (see mreGPSMq_preproc.m)
%       params:           parameters specifying back-end processing
% 
%
%   OUTPUT:
%       out:              3-dim. output matrix containing multi-res. envelope
%                         power of the corresponding auditory and modulation
%                         filter
%       out_int:          3-dim. output matrix containing multi-res power
%                         calculated on different time-scales
%       work:             some side informations (e.g. number of auditory
%                         channels), but also some additional signals which
%                         might be useful for analyzation
%       dc2mod:           3-dim output matrix containing power-based values
%                         to weight envelope power SNRs
%
% Usage:  [out out_int work dc2mod]=multResGPSM_LpFB_int_SIP_v2(input,gtfb_num,mfb_num,fs,dc_norm,work,signal_rect,params,simdef)
% author: thomas.biberger@uni-oldenburg
% date:   2018-03-18
% 
% Copyright (c) 2017-2019, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2014-2017, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------


env_pow_lim=10^-2.7;      % envelope power limit, default -27 dB



%**************dc-mod-sensitivity-trafo*************************************
%               generating a slope, for weighting modulation power with
%               respect to the DC power, e.g. modulation within an auditory
%               channel with low dc-power but above hearing threshold
%               (e.g. 10^-10) is less prominent, as modulation within an
%               auditory filter with high dc-power.
upper_lim=10^(-6.5); % -65 dB
lower_lim=10^(-10);  % -100 dB
slope=1/((10*log10(upper_lim)+100)-(10*log10(lower_lim)+100));  % +100 for trafo from -100...0dB to 0...100 dB
%*******************************************************************************


nWin=zeros(length(gtfb_num),length(mfb_num));
winSamp=zeros(length(gtfb_num),length(mfb_num));
out=zeros(ceil(length((squeeze(input(:,1,1))))/((1/mfb_num(end))*fs)),length(gtfb_num),length(mfb_num));
out_int=zeros(ceil(length((squeeze(input(:,1,1))))/((1/mfb_num(end))*fs)),length(gtfb_num),length(mfb_num));
dc2mod=ones(ceil(length((squeeze(input(:,1,1))))/((1/mfb_num(end))*fs)),length(gtfb_num),length(mfb_num));

%% ISO threshold values
if params.useISO
    vIsoThrDB = iso389_7(gtfb_num);
else
    vIsoThrDB = zeros(25,1);
end

%% Mult-Resolution Processing
for ii = params.auditory_filt_range.start:params.auditory_filt_range.end; % gtfb loop
    for kk = 1:params.mod_filt_range.end;%length(mfb_num);   % mfb loop
        
        tmp=squeeze(input(:,ii,kk));
        
        if mfb_num(kk)==0       % for dc part
            
            % taking the DC from the envelope
            tmp_pow=mean(signal_rect(ii,:));
            tmp_pow=tmp_pow^2;
            
            
            if tmp_pow<=1e-10*10^(vIsoThrDB(ii)/10);
                tmp_pow=1e-10*10^(vIsoThrDB(ii)/10);
            end
            
            out(1,ii,1)=tmp_pow;
            out_int(1,ii,1)=tmp_pow;
            nWin(ii,1)=1;
            winSamp(ii,1)=length(tmp);
            

            LpFB_out=LpFB_GPSM_v2(work.signal_rect_down(ii,:),mfb_num(1:params.mod_filt_range.end),fs);
           
            
        else    % for remaining modfilters
            
            
            % adjusting mod-filter channels to 0 dB mod-depth
            % corresponds to 1
            tmp=tmp*sqrt(2);
            
            samp_per_mfb=floor((1/mfb_num(kk))*fs);    % samples per mfb
            no_of_win=ceil(length(tmp)/samp_per_mfb);   %number of windows
            pad_samples=no_of_win*samp_per_mfb - length(tmp); % difference of samples thru temporal segmentation by windowing and the 'true' number of samples
            nWin(ii,kk)=no_of_win;
            winSamp(ii,kk)=samp_per_mfb;
            
            tmp_amp_dc_seg=LpFB_out(samp_per_mfb:samp_per_mfb:end,kk);
            
            if pad_samples ~= 0;
                tmp_amp_dc_seg=[tmp_amp_dc_seg; LpFB_out(end,kk)];
            end
            
            tmp_pow_dc_seg=tmp_amp_dc_seg.^2;
            
            %**********************************************
            % considering only auditory channels above hearing
            % threshold
            if out(1,ii,1)<=1e-10*10^(vIsoThrDB(ii)/10); % checking the global dc
                tmp_pow_ac=ones(length(tmp_pow_dc_seg),1)*env_pow_lim;
                out(1:length(tmp_pow_ac),ii,kk)=tmp_pow_ac;
            else
                %*******************************************************
                
                
                for ll= 1:no_of_win;        % temporal segmentation
                    
                    tmp_ac=tmp((ll-1)*samp_per_mfb+1:min(ll*samp_per_mfb,length(tmp)));
                    
                    if kk == 2
                        tmp_pow_ac=var(tmp_ac);
                    else
                        tmp_pow_ac=mean(tmp_ac.^2);
                    end
                    
                   
                    if mfb_num(1)~=0
                        tmp_pow=mean(signal_rect(ii,:));
                        tmp_pow=tmp_pow^2;
                    end
                    
                    if tmp_pow_dc_seg(ll)<=1e-10*10^(vIsoThrDB(ii)/10); % criterion added at 07.11.2013 by tb
                        tmp_pow_ac=env_pow_lim;  % squeeze out vorher allokieren
                        out_int(ll,ii,kk)=1e-10*10^(vIsoThrDB(ii)/10);
                    else
                        tmp_pow_ac=tmp_pow_ac/tmp_pow_dc_seg(ll);  % squeeze out vorher allokieren
                        out_int(ll,ii,kk)=tmp_pow_dc_seg(ll);
                    end
                    
             
                    if tmp_pow_dc_seg(ll) > upper_lim*10^(vIsoThrDB(ii)/10), % for mod channels above critical dc-value no penalty term
                        
                        dc2mod(ll,ii,kk)=1;
                        
                    else % for mod channels below critical dc-value penalty term
                        dc2mod(ll,ii,kk)=max(slope*(10*log10(tmp_pow_dc_seg(ll))+100-vIsoThrDB(ii)),0); % log
                        
                    end
                    tmp_pow_ac=max(tmp_pow_ac,env_pow_lim);     % lower limit of the envelope power
                    out(ll,ii,kk)=tmp_pow_ac;
                end
            end
        end
        
        work.nWin=nWin;
        work.winSamp=winSamp;
        
    end
end
