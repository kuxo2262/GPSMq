function[simdef] = mreGPSMq_model_def()
% mreGPSMq_model_def.m  this function sets parameters of the gammatone
% filterbank and the modulation filterbank and turns resampling on/off

% OUTPUT:
%       def:        struct for stimulus related parameters,e.g.,fs
%       simdef:     struct for model related parameters, e.g., filter order,
%                   cut-off frequency 
%       simwork:    struct for parameters applied during simulation, e.g.,
%                   filter coefficients
% 
% Usage: [simdef] = mreGPSMq_model_def()
% author: thomas.biberger@uni-oldenburg.de;
% date: 2013-03-07
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------

%% Resampling
simdef.resampling='on';             % set resampling 'on' or 'off' 

%% Gammatone filter variables (don't mention values below, now we are using fixed values deposited in gfb_analyzer_new_tb.m)
simdef.gt_Dens ='terz';           	% only 'terz' is a valid option
% *************************************************************************
% if simdef.gt_Dens= 'terz' is selected one ERB wide filters (as default simdef.gt_BW=1) with center frequencies of 63 80 100 125 160 200 250 315 400 500 630 800 1000
% 1250 1600 2000 2500 3150 4000 5000 6300 8000 10000 12500 16000 Hz are applied.
% Don't mind values given below for simdef.gt_MinCF, simdef.gt_MaxCF, simdef.gt_align = 1000;
% *************************************************************************
simdef.gt_MinCF = 63;               % lowest CF in Hz
simdef.gt_MaxCF = 16000;          	% highest CF in Hz (mj init 4000)
simdef.gt_align = 1000;             % 15/11/10 CI: specify base frequency around which first filter is aligned
simdef.gt_MultipleCFs = [];         % use combinations of low/upp like [100 1000 3000 4000 9000 9000] overrides min/maxCF	
simdef.gt_BW = 1.0;                 % bandwidth of the filter in ERB;default 1
%% modulation filter variables  (don't mention values below, now we are using fixed values deposited in mfb2_GPSM.m)
simdef.mf_style = 'mfb2_GPSM';          % style of modulation filterbank; currently only 'mfb2_GPSM' available 
% *************************************************************************
% if simdef.mf_style= 'mfb2_GPSM' is selected a modulation lowpass filter (cutoff freq.=1Hz)and modulation bandpass
% filters with Q-value of 1 and center frequencies of 2 4 8 16 32 64 128
% 256 Hz are applied. However, as described in Biberger et al. (2018) only
% filters ranging from 2 - 64 Hz are evaluated in the model back end.
% Don't mind values given below for simdef.mf_MinCF, simdef.mf_MaxCF, simdef.mf_gtfac;
% *************************************************************************
simdef.mf_MinCF = 0;   %           % lowest modulation filter
simdef.mf_MaxCF = 1000;            % upper most modulation filter (mj init 1000)
simdef.mf_gtfac = 0.25;            % factor for deriving the upper most mf depending on the gt_CF
simdef.mf_den = 1;                 % [1] density of the modulation filters (only 'mfb2')
simdef.mf_mfb2style = 2;        	% overall 150 Hz lowpass (only 'mfb2') 1 = enable, 2 = disable

end