function  [def simdef simwork]= mreGPSMq_param_def(def)
% mreGPSMq_param_def.m  sets some parameters of model stages and preallocates
% some values/outputs to be calculated only one time (e.g. auditory filterbank)
% 
% INPUT:        
%       def:        struct for various parameters, e.g.,fs
% 
% OUTPUT:
%       def:        struct for stimulus related parameters,e.g.,fs
%       simdef:     struct for model related parameters, e.g., filter order,
%                   cut-off frequency 
%       simwork:    struct for parameters applied during simulation, e.g.,
%                   filter coefficients
% 
% Usage:  [def simdef simwork]= mreGPSMq_param_def(def)
% author: thomas.biberger@uni-oldenburg.de;
% date:   2016-11-05
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Thomas Biberger, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------


%% signal pre-processing

    [simdef]=mreGPSMq_model_def();     % set parameters of model stages (e.g. spacing of auditory filters, filter order)
    [def simdef simwork]= mreGPSMq_init(def,simdef);          % preallocates some values/outputs that must be computed one time only (e.g. auditory filterbank)
    
end
