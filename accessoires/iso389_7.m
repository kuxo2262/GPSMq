function [vIsoThrDB, vsF] = iso389_7(vsDesF)
% iso389_7.m    based on the script "isothr(vsDesF)" from Jens-E. Appell 
% values from 20 Hz to 18000 Hz are taken from ISO 389-7 Akustik - Standard-Bezugspegel fur die Kalibrierung von % audiometrischen Ger�ten - Teil 7: Bezugsh�rschwellen unter Freifeld- undo Diffusfeldbedingungen
% values at 0 and 20000 Hz are not taken from ISO Threshold contour !!
% plot added by N. Harlander 12.10.11

%  INPUT:
%       vsDesF:                vector containing frequencies of the input
%                              signal
%
%   OUTPUT:
%       vIsoThrDB:            frequency-dependent ISO-threshold in dB                           
%                             
%       vsF:                  frequency vector corresponding to the ISO-threshold
%                             values
% 
% 
% Usage: [vIsoThrDB, vsF] = iso389_7(vsDesF)
% author: thomas.biberger@uni-oldenburg
% date: 2013-09-25
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Thomas Biberger, Jan-Hendrik Fle�ner, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------

vThr = [78.5  78.5 68.7  59.5 51.1 44.0 37.5  31.5 26.5 22.1 ...
           17.9 14.4 11.4 8.6   6.2  4.4 3.0  2.4 2.2 2.4  3.5 2.4 ...
           1.7 -1.3 -4.2 -5.8 -6.0 -5.4 -1.5 4.3 6.0 12.6 13.9 13.9 ...
           13.0 12.3 18.4 40.2 73.2 73.2]';
  vsF  =1000*[0   0.02 0.025 0.0315 0.04 0.05 0.063 0.08 0.1 ...
              0.125 0.16 0.2  0.25 0.315 0.4 0.5 0.63 0.75 0.8 1.0 1.25 ...
              1.5  1.6  2.0  2.5  3.0  3.15 4.0  5.0  6.0  6.3 8.0  9.0 ...
              10.0 11.2  12.5   14.0 16.0  18.0    20.0]';
  if nargin > 0,
      if( ~isempty( find( vsDesF < 20 ) ) )
%         warning('frequency values below 20 Hz set to 20 Hz');
        vsDesF(find( vsDesF < 20 )) = 20;
      end
      vIsoThrDB = interp1(vsF,vThr,vsDesF,'linear','extrap'); %default
%         vIsoThrDB = interp1(vsF,vThr,vsDesF,'spline','extrap');

      vsF = vsDesF;
  else
    vIsoThrDB = vThr;
  end;
   
   
% figure;
% semilogx(vsDesF,vIsoThrDB,'k-o','MarkerSize',1,'LineWidth',2)
% axis([0 23000 -10 100])
% ylabel('Amplitude [dB SPL]','FontSize',12);
% xlabel('Frequency','FontSize',12);
% title('ISO 389-7 (April 2006) Hearing Threshold','FontSize',12);
% set(gca,'FontSize',12)
% box off
% grid on
