function  [RefSig TestSig def]= GPSMq_preproc_input(RefSig,TestSig,fs,tempAlign)
% GPSMq_preproc_input.m  applies some pre-processing to the reference and
% test signal

% INPUT:        
%       RefSig:     unprocessed signal -> reference signa
%       TestSig:    processed signal -> test signal
%       fs          sampling frequency
%       tempAlign   temporal alignment on (1) or off (0)

% OUTPUT:
%       RefSig:     unprocessed signal after pre-proc
%       TestSig:    processed signal after pre-proc
%       def:        struct for various parameters, e.g.,fs


% Usage: [RefSig TestSig def]= GPSMq_preproc_input(RefSig,TestSig,fs,tempAlign)
% author: thomas.biberger@uni-oldenburg.de;
% date: 2016-11-05
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Thomas Biberger, Jan-Hendrik Fle�ner, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------

%% signal pre-processing

% ensure equal lengths
minlen = min([length(TestSig), length(RefSig)]);
TestSig = TestSig(1:minlen,:);
% TestSig = TestSig(:);
RefSig = RefSig(1:minlen,:);
% RefSig = RefSig(:);

% temporal alignment
if tempAlign
    [val idx]=max(xcorr(RefSig,TestSig));
    if idx<=minlen;
        TestSig=TestSig(length(TestSig)-idx+1:end,1);
        RefSig=RefSig(1:end-(length(RefSig)-length(TestSig)),1);
    elseif idx>minlen;
        RefSig=RefSig(idx-length(RefSig)+1:end,1);
        TestSig=TestSig(1:end-(length(TestSig)-length(RefSig)),1);
    else
    end
end

def=struct(...
    'samplerate',fs, ...               % sampling freq. in Hz
    'intervallen',length(RefSig) ...               % signal length in samples
    );

end
