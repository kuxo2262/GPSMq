% EXAMPLE (OCTAVE) for the usage of the GPSMq that is described in Biberger et al.
% (2018). The GPSMq is a reference-based audio quality model and based on
% power and envelope power SNRs to predict subjective audio quality ratings. 
% A stimuli level of 0 dB full scale rms (dB FS rms, which means 1 in Matlab)
% is assumed to represent a sound pressure level of 100 dB SPL rms.
% Example: stimulus (sound) calibration to 65 dB SPL rms; 
% sound_adapt=sound/rms(sound) * 10^((65-100)/20);

pkg load signal;
pkg load statistics;

cur_dir=pwd;
addpath(genpath(cur_dir)); % include all folders from the current path
%% input signals


%% input signals
v = version;        % octave version as string
v = str2num(v(1));  % first version number
if v < 4
    % reference signal (clean)
    [RefSig, fsRef] = wavread('Stimuli/Ref.wav');
    % test signal (processed)
    [TestSig, fsTest] = wavread('Stimuli/Test.wav');
else
    % reference signal (clean)
    [RefSig, fsRef] = audioread('Stimuli/Ref.wav');
    % test signal (processed)
    [TestSig, fsTest] = audioread('Stimuli/Test.wav');
end


% compare sampling frequencies
if fsTest ~= fsRef,
    error('signals have different sampling frequencies')
else
    fs = fsTest;
end

minlen = min([length(TestSig), length(RefSig)]);
TestSig = TestSig(1:minlen,1);
RefSig = RefSig(1:minlen,1);

% calculate objective perceptual measure
stOut = GPSMq(RefSig, TestSig, fs);
disp(stOut)

%        OPM: 14.064
%    OPM_raw: 10.064
%     SNR_dc: 10.101
%     SNR_ac: 0.0473