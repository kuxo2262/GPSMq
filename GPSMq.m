function stOut = GPSMq(RefSig, TestSig, fs)
% GPSMq     Main function for calculation of audio quality differences between an 
%           unprocessed reference signal and a processed test signal. At first model
%           parameters are defined, followed by the front end and back end
%           calculations resulting in the objective perceptual measure. For
%           more detailed information see Biberger et al. (2018)
% 
% input:    RefSig   ... reference signal, 1-dim
%           TestSig  ... test signal, 1-dim, time-aligned with RefSig
%           fs       ... sampling frequency
%
% output:   stOut    ... struct that contains the objective perceptual measure (OPM) with 
%                        upper and lower limits as described in Biberger et al. (2018),
%                        the objective perceptual measure without limits (OPM_raw)
%                        intensity SNRs (SNR_dc), and envelope power SNRs
%                        (SNR_ac)
%                        
%                         
% Usage: stOut = GPSMq(RefSig, TestSig, fs)
% author: thomas.biberger@uni-oldenburg.de
% updated 2018-06-23
% 
% --------------------------------------------------------------------------------
% Copyright (c) 2017-2019, Thomas Biberger, Jan-Hendrik Fle�ner, Stephan D. Ewert,
% University Oldenburg, Germany.
%
% This work is licensed under the
% Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
% License (CC BY-NC-ND 4.0).
% To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
% Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
% 94041, USA.
% --------------------------------------------------------------------------------


% Back-end related paramter settings
params.auditory_filt_range.start=8;             % audio freqs=[63 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 10000 12500 16000]
params.auditory_filt_range.end=24;              % 
params.mod_filt_range.start=3;                  % 3 filter range of modulation filters 2: 1 Hz modulation channel , 10: 256 Hz modulation channel
params.mod_filt_range.end=8;                    % 8 filter range of modulation filters 2: 1 Hz modulation channel , 10: 256 Hz modulation channel
params.useISO=1;                                % 1: use ISO threshold middle ear filter, 0: do not

%% signal pre-processing (check sig. length, temporal alignment)
tempAlign = 0;
[RefSig TestSig def]= GPSMq_preproc_input(RefSig,TestSig,fs,tempAlign);
work.signal=[RefSig TestSig]; 

%% set model params
[def simdef simwork]=mreGPSMq_param_def(def);

%% Front end calculation
[Ref]=mreGPSMq_preproc(RefSig,def,simdef,simwork,params);   % input for reference signal (work.signal(:,1)) (best quality)
[Test]=mreGPSMq_preproc(TestSig,def,simdef,simwork,params);   % input for manipulated signal (work.signal(:,2))

%% Back end calculation
[BE]=frommat2single_STint_mreGPSMq(Ref,Test,params);
[OPM,OPM_raw,SNR_dc,SNR_ac]=BackEnd_feature_trafo_mreGPSMq(BE,params);

%% Model output
stOut.OPM = OPM;
stOut.OPM_raw = OPM_raw;
stOut.SNR_dc = SNR_dc;
stOut.SNR_ac = SNR_ac;

