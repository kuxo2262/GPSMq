The intrusive audio quality model GPSMq (Biberger et al., 2018) requires the clean 
and the distorted signal as input. Clean and distorted signals have to be temporal 
aligned. (A temporal alignment based on cross-correlation can be used by setting the 
value of the variable 'tempAlign' within the function 'GPSMq.m' from 0 to 1.)
It is recommended that the utilized sound signals have sampling rates higher than
30 kHz (e.g. 44100 kHz).

'example_GPSMq.m' and 'example_GPSMq_octave' give minimal examples how to use GPSMq in Matlab and Octave. 

The GPSMq output provides four submeasures:
'OPM':          objective perceptual measure; based on a combination of 'SNR_dc' and 'SNR_ac' 
                to which a logarithmic transformation with an lower and upper boundary is applied.
                Distortions resulting in SNRs below the lower boundary are assumed to be
                imperceptible, while distortions causing large SNRs that exceed the upper limit are
                assumed to lead to a fixed (poor) quality.
'OPM_raw':      identical to 'OPM' but without lower and upper boundary
'SNR_dc':       power-based SNR; based on temporal averaging and combination across auditory channels
'SNR_ac':       envelope-power-based SNR; based on temporal averaging and combination across auditory and
                modulation channels


A more detailed description of GPSMq is given in:

T. Biberger, J.-H. Fle�ner, R. Huber, and S. D. Ewert, "An objective audio quality 
measure based on power and envelope power cues", Journal of the Audio Engineering Society,
vol. 66, no.7/8, PP.1-16. 2018.https://doi.org/10.17743/jaes.2018.0031

Abstract:
The generalized power spectrum model [GPSM; Biberger and Ewert (2016), J. Acoust. Soc.
Am., 140, 1023-1038], which has been shown to account for a large number of psychoacoustic
and speech intelligibility (SI) experiments, was extended to assess audio quality. Like
the GPSM, the suggested audio quality model, GPSMq, combines features from the powerspectrum
model (PSM) [Patterson and Moore (1986) in Frequency Selectivity in Hearing,
123-177] and envelope power-spectrum model (EPSM) [Ewert and Dau (2000), J. Acoust.
Soc. Am., 108, 1181-1196]. GPSMq utilizes signal-to-noise ratios (SNRs) in the power and
envelope power domains to model the addition or removal of energy by the signal processing
under test. Four audio quality databases, introducing linear and nonlinear distortions to music
and speech signals, were assessed to cover a large variety of distortions. GPSMq provided
better overall prediction performance than other state-of-the-art auditory-model-based objective
quality measures. The results demonstrate that the power and envelope power SNR metric
is appropriate for predicting audio quality for a variety of signal distortions in addition to
psychoacoustics and SI. This supports the notion that the auditory system extracts a universal
set of auditory features to be analyzed in a task-dependent decision stage.


Authors of the Matlab implementation of GPSMq: 
thomas.biberger@uni-oldenburg.de;
jan-hendrik.flessner@uni-oldenburg.de


===============================================================================
License and permissions
===============================================================================

Unless otherwise stated, the GPSMq distribution, including all files is licensed
under Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International
(CC BY-NC-ND 4.0).
In short, this means that you are free to use and share (copy, distribute and
transmit) the GPSMq distribution under the following conditions:

Attribution - You must attribute the GPSMq distribution by acknowledgement of
              the author if it appears or if was used in any form in your work.
              The attribution must not in any way that suggests that the author
              endorse you or your use of the work.

Noncommercial - You may not use GPSMq for commercial purposes.
 
No Derivative Works - You may not alter, transform, or build upon GPSMq.

Exceptions are the following external Matlab functions (see their respective licence)
that were used within the GPSMq:
- Gammatone filterbank from V. Hohmann (https://zenodo.org/record/2643400#.XQsf5TnVLCM), for details see[1,2]:
   [1] Hohmann, V. (2002). Frequency analysis and synthesis using a Gammatone filterbank. 
       Acta Acustica united with Acustica, 88(3), 433-442.
   [2] Herzke, T., & Hohmann, V. (2007). Improved numerical methods for gammatone filterbank analysis and
       synthesis. Acta acustica united with acustica, 93(3), 498-500. 
- 'MFB2.m' from Stephan D. Ewert and T. Dau
- 'moving_average.m' from Christian Kothe (Code available at Matlab's File Exchange
   https://www.mathworks.com/matlabcentral/fileexchange/34567-fast-moving-average) 

